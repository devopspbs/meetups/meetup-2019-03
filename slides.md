<!-- .slide: data-background-image="images/meetup-2019-03-30.jpg" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

## Recados

- **Happy hour**
- **Coffe break**
- **"Senha do Wi-Fi"**

----  ----

## O Que Temos Para Hoje

- **15:30 as 15:40** - Abertura
- **15:40 as 16:20** - [Análise de Dados com o Power BI - Dhony Silva](https://gitlab.com/devopspbs/meetup-2019-03/raw/master/presentations/01_DevOpsPbs_PowerBI_Analise_de_dados_publicos.pptx)
- **16:20 as 16:40** - Coffee Break
- **16:40 as 17:20** - [O Marco Civil da Internet - Allan Glauber](https://gitlab.com/devopspbs/meetup-2019-03/raw/master/presentations/Direito%20Digital%20-%20Allan%20Glauber.pptx)
- **17:20 as 18:00** - Painel: Comunidade e Eventos
- **18:00 as 18:05** - Encerramento

----  ----

<a href="https://www.meetup.com/devopspbs/"><img width="240" data-src="images/DEVOLPBS.png" alt="DevOpsPBS"></a>

Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas (Desenvolvedores, Sysadmins, Infosecs, QAs, Engs/Admins de rede, Técnicos de informática, Analistas, Web Designers, etc)

----  ----

## Site e Mídias Sociais

* **Site oficial**
  - Em breve

* **Mídias sociais**
  - [meetup.com/devopspbs](https://www.meetup.com/devopspbs/)
  - [gitlab.com/devopspbs](https://gitlab.com/devopspbs)

* **Grupos de discussão**
  - Grupo [DevOpsPBS](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ) no Telegram
  - Grupo [TECNOLOGIA PBS](https://chat.whatsapp.com/7XfTiu53icSKKbANTQnMGH) no Whatsapp

----  ----

## Patrocinadores & Apoio

<a href="https://www.instagram.com/designbrindespa/"><img height="90" data-src="images/DESIGNBRINDES.png" alt="Design Brindes"></a>
&emsp; <a href="http://pontocompa.com.br/"><img height="90" data-src="images/ponto-com.svg" alt="Ponto Com"></a>


<a href="http://www.nortetelecom.net.br/"><img height="90" data-src="images/norte-telecom.png" alt="Norte Telecom"></a>
&emsp; <a href="http://www.nortetecnologia.com.br/"><img height="90" data-src="images/norte-tecnologia.svg" alt="Norte Tecnologia"></a>


<a href="http://www.parauapebas.pa.gov.br/"><img height="90" data-src="images/parauapebas.svg" alt="Parauapebas"></a>
&emsp; <a href="https://parauapebas.ifpa.edu.br/"><img height="90" data-src="images/IFPA.svg" alt="IFPA"></a>

----  ----

## Dica: Linkedin Mobile

![](images/linkedin.png)
