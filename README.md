# Meetup Março 2019

As apresentações estão disponíveis no diretório `presentations`.

## Build com Docker

Com uma instalação funcional do Docker mais docker-compose basta rodar:

```
docker-compose up
```

Os slides estarão disponíveis no diretório `presentations`.

## License

GPLv3 or later.
